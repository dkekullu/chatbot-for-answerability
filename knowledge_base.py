#!/usr/bin/python
#

import sys
import os
import json
import codecs

def main():
    print('The scripts has been called!!')

def getReasonsForUser(user, intent):
    print('Get data from knowledge base.')
    file_name = user+'.json'
    reasons = readJsonFromFile(file_name)
    return reasons[intent]

def readJsonFromFile(file_name):
    knowledge_file = codecs.open(file_name,'r',encoding='utf8')
    return json.load(knowledge_file)

if __name__ == '__main__':
    main()