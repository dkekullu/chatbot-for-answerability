# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions



from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import human_representative
import knowledge_base

class ActionReasonTreatment(Action):

    def name(self) -> Text:
        return "action_reason_treatment"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        user_message = knowledge_base.getReasonsForUser('kathryn','treatment')
        dispatcher.utter_message(text=user_message)

        return []

class ActionReasonRisk(Action):

    def name(self) -> Text:
        return "action_reason_risk"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        user_message = knowledge_base.getReasonsForUser('kathryn','risk')
        dispatcher.utter_message(text=user_message)

        return []


class ActionReasonScore(Action):

    def name(self) -> Text:
        return "action_explain_score"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        user_message = knowledge_base.getReasonsForUser('kathryn','score')
        dispatcher.utter_message(text=user_message)

        return []

class ActionWrongInfo(Action):

    def name(self) -> Text:
        return "action_wrong_info"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="You stated that the information was wrong. I am connecting you to our representative for information update.")
        human_representative.connect('wrong_info')
        

        return []

class ActionConnectHuman(Action):

    def name(self) -> Text:
        return "action_connect_human"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="You want to connect with our representative. I am connecting you to them.")
        human_representative.connect('request')
        

        return []

class ActionUnknownQuestion(Action):

    def name(self) -> Text:
        return "action_unknown_question"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="I'm sorry, I cannot answer that question. I will connect you to our human representative.")
        human_representative.connect('unknown_question')
        

        return []