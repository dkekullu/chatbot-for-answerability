# Chatbots for Answerable Sociotechnical Systems

This is a proof-of-concept chatbot implementation for answerable sociotechnical systems. 

Autonomous decisions affect our lives increasingly in ways that are unprecedented. Organizations using AI-based systems should be able to explain the reasoning behind the specific actions and provide their users a platform to understand the actions and challenge them, if needed. We propose a hybrid system with automated chatbots and assigned human representatives. This will ensure that the answers are given in a timely manner when they can be provided by the chatbot and human representatives who are responsible regarding the automated decisions will be ready to handle more complicated processes.

## SCENARIO
Kathryn suffers from endometriosis. During her hospital stay, she is provided opioid medication. One day a staffer brusquely informs Kathryn that she would no longer be receiving any kind of opioid without providing any explanations. She later discovers that her pet's opioid medication was listed under her name, which resulted in her high risk score. [Full story](https://www.wired.com/story/opioid-drug-addiction-algorithm-chronic-pain/) 

In our example, the chatbot starts with greetings and give sample questions Kathryn can ask. Kathryn can also start asking the questions from the start. The chatbot answers them using its knowledge base. When Kathryn states that the information around prescriptions is wrong, the chatbot triggers custom action to connect to a human representative. A story scenario with (intent, action) pairs for this example is as following: (ask\_treatment\_stop, action\_reason\_treatment), (why\_at\_risk,  action\_reason\_risk), (ask\_score\_system, action\_explain\_score), (state\_wrong\_info, action\_wrong\_info).

<img src="scenario_pictures/chatbot_1.png"  width="240" height="300"> <img src="scenario_pictures/chatbot_2.png"  width="240" height="300"> <img src="scenario_pictures/chatbot_3.png"  width="240" height="300">

## RUNNING

- Create and activate virtual environment
- Install Rasa Chatbot Framework - [Rasa](https://rasa.com/)
- Clone the repository
- Start the rasa bot with command: rasa run --enable-api --cors "*"
- Start the custom actions server in another terminal: rasa run actions --cors "*"
- Open index.html in ui folder and start interacting with the chatbot. UI component is taken from project [here](https://github.com/anishmahapatra/01_rasa_color_cb).
